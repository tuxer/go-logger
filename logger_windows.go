package log

/**
 * Created by tuxer on 11/13/17.
 */

const (
    clear       = ""

    fgWhite    = ""
    fgRed      = ""
    fgGreen    = ""
    fgYellow   = ""
    fgBlue     = ""
    fgMagenta  = ""
    fgCyan     = ""
    fgBlack    = ""

    bgWhite    = ""
    bgRed      = ""
    bgGreen    = ""
    bgYellow   = ""
    bgBlue     = ""
    bgMagenta  = ""
    bgCyan     = ""
    bgBlack    = ""

)
